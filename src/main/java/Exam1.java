
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class Exam1 {
    // A-1.8
    // This code have no even and odd check
    // cause of loop is integer
    // Ex. 3/2 = 1 ,2/2 = 2

    public static void main(String[] args) throws IOException {
        
        //startTime
        long startTime = System.nanoTime();
        
        //ReadFile
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();
        // System.out.println(input);

        String[] A = input.split(" ");

        String temp;

        for (int i = 0; i < A.length / 2; i++) {
            temp = A[i];
            A[i] = A[A.length - i - 1];
            A[A.length - i - 1] = temp;
        }

        // print array A
        for (String arrOutput : A) {
            System.out.print(arrOutput + " ");
        }
        
        //endTime
        long endTime = System.nanoTime();
        long totalTime =  endTime-startTime;
        
        double Time = (double)totalTime/1000000000;
        System.out.println("\nTotal time : "+Time+" s");
    }
}
