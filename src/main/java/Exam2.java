
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class Exam2 {

    public static void main(String[] args) throws IOException {
        //FileReader fr = new FileReader("C:\\Algorithm\\ExamAlgorithm\\src\\main\\java\\Exam2_Input10.txt");

        //startTime
        long startTime = System.nanoTime();

        Scanner kb = new Scanner(System.in);

        int n = kb.nextInt();
        int[] A = new int[n];

        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }

        longestSubarray(n, A);
        
        //endTime
        long endTime = System.nanoTime();
        long totalTime =  endTime-startTime;
        
        double Time = (double)totalTime/1000000000;
        System.out.println("\nTotal time : "+Time+" s");

    }

    public static void longestSubarray(int n, int[] A) {
        int max = 1;
        int lenght = 1;

        int start = 0;
        int end = 0;

        for (int i = 1; i < n; i++) {
            if (A[i] >= A[i - 1]) {
                lenght++;
            } else {
                lenght = 1;
            }
            if (max < lenght) {
                max = lenght;
                end = i;
            }
        }

        start = end - (max - 1);

        //show longes subarray 
        System.out.println("lenght of longes subarray : " + max);
        System.out.println("longes subarray : ");
        for (int i = start; i <= end; i++) {
            System.out.print(A[i] + " ");
        }

    }
}
